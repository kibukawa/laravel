<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    //シリーズ取得関数
    public function getSeries(){
        //JSON形式で取得
        $series  = DB::table('series')
            ->select('id','name','sequence')
            ->orderBy('sequence','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($series);
    }

    //シリーズ登録/更新関数
    public function setSeries(){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $sequence = $_REQUEST['sequence'];

        //バリデーションチェック(未実装)

        //登録
        if(empty($id)){
            DB::table('series')
                ->insert([
                    'name'      => $name,
                    'sequence'  => $sequence
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if(!empty($name)){
                DB::table('series')
                    ->where('id',$id)
                    ->update([
                        'name'      => $name,
                        'sequence'  => $sequence
                    ]);
            }

            //削除
            if(empty($name)){
                DB::table('series')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }

    //タイトル(software)取得関数
    public function getSoftware(){
        $series_id = $_REQUEST['series_id'];

        //JSON形式で取得
        $software  = DB::table('software')
            ->select('id','name','sequence')
            ->where('series_id',$series_id)
            ->orderBy('sequence','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($software);
    }

    //タイトル(Software)登録/更新関数
    public function setSoftware(){
        $id = $_REQUEST['id'];
        $series_id = $_REQUEST['series_id'];
        $name = $_REQUEST['name'];
        $sequence = $_REQUEST['sequence'];

        //バリデーションチェック(未実装)

        //登録
        if(empty($id)){
            DB::table('software')
                ->insert([
                    'series_id' => $series_id,
                    'name'      => $name,
                    'sequence'  => $sequence
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if(!empty($name)){
                DB::table('software')
                    ->where('id',$id)
                    ->update([
                        'series_id' => $series_id,
                        'name'      => $name,
                        'sequence'  => $sequence
                    ]);
            }
            //削除
            if(empty($name)){
                DB::table('software')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }

    //ハードウェア取得関数
    public function getHardware(){
        //JSON形式で取得
        $hardware  = DB::table('hardware')
            ->select('id','name','sequence')
            ->orderBy('sequence','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($hardware);
    }

    //ハードウェア登録/更新関数
    public function setHardware(){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $sequence = $_REQUEST['sequence'];

        //バリデーションチェック(未実装)

        //登録
        if(empty($id)){
            DB::table('hardware')
                ->insert([
                    'name'      => $name,
                    'sequence'  => $sequence
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if(!empty($name)){
                DB::table('hardware')
                    ->where('id',$id)
                    ->update([
                        'name'      => $name,
                        'sequence'  => $sequence
                    ]);
            }

            //削除
            if(empty($name)){
                DB::table('hardware')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }

    //攻略本取得関数
    public function getBook(){
        //JSON形式で取得
        $hardware  = DB::table('book')
            ->select('id','name','sequence')
            ->orderBy('sequence','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($hardware);
    }

    //攻略本登録/更新関数
    public function setBook(){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $sequence = $_REQUEST['sequence'];

        //バリデーションチェック(未実装)

        //登録
        if(empty($id)){
            DB::table('book')
                ->insert([
                    'name'      => $name,
                    'sequence'  => $sequence
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if(!empty($name)){
                DB::table('book')
                    ->where('id',$id)
                    ->update([
                        'name'      => $name,
                        'sequence'  => $sequence
                    ]);
            }

            //削除
            if(empty($name)){
                DB::table('book')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }

    //サウンドウェア取得関数
    public function getSoundware(){
        //JSON形式で取得
        $soundware  = DB::table('soundware')
            ->select('id','name','sequence')
            ->orderBy('sequence','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($soundware);
    }

    //サウンドウェア登録/更新関数
    public function setSoundware(){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $sequence = $_REQUEST['sequence'];

        //バリデーションチェック(未実装)

        //登録
        if(empty($id)){
            DB::table('soundware')
                ->insert([
                    'name'      => $name,
                    'sequence'  => $sequence
                ]);
        }

        //更新
        if(!empty($id)){
            DB::table('soundware')
                ->where('id',$id)
                ->update([
                    'name'      => $name,
                    'sequence'  => $sequence
                ]);
        }
    }

    //ソフトウェア--ハードウェア取得関数
    public function getSoftwareHardware(){
        $software_id = $_REQUEST['software_id'];
        //JSON形式で取得
        $softwareHardware  = DB::table('software_hardware as sh')
            ->join('hardware as h','sh.hardware_id','=','h.id')
            ->select('sh.id','sh.software_id','sh.hardware_id','sh.item_code','sh.price','sh.release_date','sh.remark','h.name')
            ->where('software_id',$software_id)
            ->orderBy('release_date','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($softwareHardware);
    }

    //ソフトウェア--ハードウェア登録/更新関数
    public function setSoftwareHardware(){
        $id             = $_REQUEST['id'];
        $software_id    = $_REQUEST['software_id'];
        $hardware_id    = $_REQUEST['hardware_id'];
        $item_code      = isset($_REQUEST['item_code']) ? $_REQUEST['item_code'] : '';
        $price          = isset($_REQUEST['price']) ? (int)$_REQUEST['price'] : 0;
        $release_date   = !empty($_REQUEST['release_date']) ? date('Y-m-d',strtotime($_REQUEST['release_date'])) : '2038-01-01';
        $remark         = isset($_REQUEST['remark']) ? $_REQUEST['remark'] : '';

        //バリデーションチェック(未実装)
        //登録
        if(empty($id)){
            DB::table('software_hardware')
                ->insert([
                    'software_id'   => $software_id,
                    'hardware_id'   => $hardware_id,
                    'item_code'     => $item_code,
                    'price'         => (int)$price,
                    'release_date'  => $release_date,
                    'remark'        => $remark,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s')
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if($hardware_id > 0){
                DB::table('software_hardware')
                    ->where('id',$id)
                    ->update([
                        'software_id'   => $software_id,
                        'hardware_id'   => $hardware_id,
                        'item_code'     => $item_code,
                        'price'         => $price,
                        'release_date'  => $release_date,
                        'remark'        => $remark
                    ]);
            }

            //削除
            if($hardware_id == 0){
                DB::table('software_hardware')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }

    //ソフトウェア--攻略本取得関数
    public function getSoftwareBook(){
        $software_id = $_REQUEST['software_id'];
        //JSON形式で取得
        $softwareBook  = DB::table('software_book as sb')
            ->join('book as b','sb.book_id','=','b.id')
            ->select('sb.id','sb.software_id','sb.book_id','sb.item_code','sb.price','sb.release_date','sb.remark','b.name')
            ->where('software_id',$software_id)
            ->orderBy('release_date','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($softwareBook);
    }

    //ソフトウェア--攻略本登録/更新関数
    public function setSoftwareBook(){
        $id             = $_REQUEST['id'];
        $software_id    = $_REQUEST['software_id'];
        $book_id    = $_REQUEST['book_id'];
        $item_code      = isset($_REQUEST['item_code']) ? $_REQUEST['item_code'] : '';
        $price          = isset($_REQUEST['price']) ? $_REQUEST['price'] : '';
        $release_date   = !empty($_REQUEST['release_date']) ? date('Y-m-d',strtotime($_REQUEST['release_date'])) : '2038-01-01';
        $remark         = isset($_REQUEST['remark']) ? $_REQUEST['remark'] : '';

        //バリデーションチェック(未実装)
        //登録
        if(empty($id)){
            DB::table('software_book')
                ->insert([
                    'software_id'   => $software_id,
                    'book_id'       => $book_id,
                    'item_code'     => $item_code,
                    'price'         => (int)$price,
                    'release_date'  => $release_date,
                    'remark'        => $remark,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s')
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if($book_id > 0){
                DB::table('software_book')
                    ->where('id',$id)
                    ->update([
                        'software_id'   => $software_id,
                        'book_id'       => $book_id,
                        'item_code'     => $item_code,
                        'price'         => (int)$price,
                        'release_date'  => $release_date,
                        'remark'        => $remark
                    ]);
            }

            //削除
            if($book_id==0){
                DB::table('software_book')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }

    //ソフトウェア--サウンドウェア取得関数
    public function getSoftwareSoundware(){
        $software_id = $_REQUEST['software_id'];
        //JSON形式で取得
        $softwareSoundware  = DB::table('software_soundware as ss')
            ->join('soundware as s','ss.soundware_id','=','s.id')
            ->select('ss.id','ss.software_id','ss.soundware_id','ss.item_code','ss.price','ss.release_date','ss.remark','s.name')
            ->where('software_id',$software_id)
            ->orderBy('release_date','asc')
            ->get()
            ->toJSON();
        //JSON形式で出力
        print($softwareSoundware);
    }

    //ソフトウェア--サウンドウェア登録/更新関数
    public function setSoftwareSoundware(){
        $id             = $_REQUEST['id'];
        $software_id    = $_REQUEST['software_id'];
        $soundware_id   = $_REQUEST['soundware_id'];
        $item_code      = isset($_REQUEST['item_code']) ? $_REQUEST['item_code'] : '';
        $price          = isset($_REQUEST['price']) ? $_REQUEST['price'] : '';
        $release_date   = !empty($_REQUEST['release_date']) ? date('Y-m-d',strtotime($_REQUEST['release_date'])) : '2038-01-01';
        $remark         = isset($_REQUEST['remark']) ? $_REQUEST['remark'] : '';

        //バリデーションチェック(未実装)

        //登録
        if(empty($id)){
            DB::table('software_soundware')
                ->insert([
                    'software_id'   => $software_id,
                    'soundware_id'  => $soundware_id,
                    'item_code'     => $item_code,
                    'price'         => (int)$price,
                    'release_date'  => $release_date,
                    'remark'        => $remark,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s')
                ]);
        }

        //更新/削除
        if(!empty($id)){
            //更新
            if($soundware_id > 0){
                DB::table('software_soundware')
                    ->where('id',$id)
                    ->update([
                        'software_id'   => $software_id,
                        'soundware_id'  => $soundware_id,
                        'item_code'     => $item_code,
                        'price'         => (int)$price,
                        'release_date'  => $release_date,
                        'remark'        => $remark,
                        'updated_at'    => date('Y-m-d H:i:s')
                    ]);
            }

            //削除
            if($soundware_id==0){
                DB::table('software_soundware')
                    ->where('id',$id)
                    ->delete();
            }
        }
    }
}
