<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //管理ページTOP
    public function index(){
        return view('admin/index');
    }
}
