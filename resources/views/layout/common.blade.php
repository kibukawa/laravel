<!doctype html>
<html lang="{{ app()->getLocale() }}" ng-app="myApp">
<head>
    @yield("head")
</head>
<body ng-controller="MyController">
@yield("header")
{{--
<div>
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
            @endauth
        </div>
    @endif
--}}
    <main>
        @yield('content')
    </main>
{{--
</div>
--}}
@yield("footer")
</body>
</html>
