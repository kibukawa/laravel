<div class="edit_area" ng-show="editTable==2">
    {{-- ブックテーブル --}}
    <table>
        <thead>
        <tr>
            <th>攻略本名</th>
            <th>順番</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="book in books">
            <td class="name"><input type="text" class="form-control" ng-model="book.name" ng-blur="setBook(book.id)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="book.sequence" ng-blur="setBook(book.id)"></td>
        </tr>
        <tr>
            <td class="name"><input type="text" class="form-control" ng-model="bookNameNew" ng-blur="setBook(0)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="bookSequenceNew" ng-blur="setBook(0)"></td>
        </tr>
        </tbody>
    </table>
</div>
