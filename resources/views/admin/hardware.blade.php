<div class="edit_area" ng-show="editTable==1">
    {{-- ハードウェアテーブル --}}
    <table id="hardware">
        <thead>
        <tr>
            <th>ハード名</th>
            <th>順番</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="hardware in hardwares">
            <td class="name"><input type="text" class="form-control" ng-model="hardware.name" ng-blur="setHardware(hardware.id)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="hardware.sequence" ng-blur="setHardware(hardware.id)"></td>
        </tr>
        <tr>
            <td class="name"><input type="text" class="form-control" ng-model="hardwareNameNew" ng-blur="setHardware(0)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="hardwareSequenceNew" ng-blur="setHardware(0)"></td>
        </tr>
        </tbody>
    </table>
</div>
