<div class="edit_area" ng-show="editTable==3">
    {{-- サウンドウェアテーブル --}}
    <table>
        <thead>
        <tr>
            <th>サウンドシリーズ名</th>
            <th>順番</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="soundware in soundwares">
            <td class="name"><input type="text" class="form-control" ng-model="soundware.name" ng-blur="setSoundware(soundware.id)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="soundware.sequence" ng-blur="setSoundware(soundware.id)"></td>
        </tr>
        <tr>
            <td class="name"><input type="text" class="form-control" ng-model="soundwareNameNew" ng-blur="setSoundware(0)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="soundwareSequenceNew" ng-blur="setSoundware(0)"></td>
        </tr>
        </tbody>
    </table>
</div>
