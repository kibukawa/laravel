@extends('layout.common')
@include('layout.head')
@include('layout.header')
@include('layout.footer')

@section('content')
    <div id="admin">
        <div id="button_area">
            {{-- エディットエリア切り替えボタン --}}
            <button class="btn btn-danger" ng-click="editTable=0">ソフト</button>
            <button class="btn btn-info" ng-click="editTable=1">ハード</button>
            <button class="btn btn-success" ng-click="editTable=2">攻略本</button>
            <button class="btn btn-primary" ng-click="editTable=3">サウンド</button>
        </div>
        {{-- シリーズ/ソフトウェア --}}
        @include('admin/software')
        {{-- ハードウェア --}}
        @include('admin/hardware')
        {{-- 攻略本 --}}
        @include('admin/book')
        {{-- サウンドウェア --}}
        @include('admin/soundware')
    </div>
@endsection
