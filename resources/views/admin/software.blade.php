<div class="edit_area" ng-show="!editTable">
    {{-- シリーズテーブル --}}
    <table id="series" ng-show="!series_id">
        <thead>
        <tr>
            <th>シリーズ名</th>
            <th>順番</th>
            <th>タイトル名</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="series in serieses">
            <td class="name"><input type="text" class="form-control" ng-model="series.name" ng-blur="setSeries(series.id)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="series.sequence" ng-blur="setSeries(series.id)"></td>
            <td class="software"><button class="btn btn-warning" ng-click="getSoftware(series.id)">タイトル名</button></td>
        </tr>
        <tr>
            <td class="name"><input type="text" class="form-control" ng-model="seriesNameNew" ng-blur="setSeries(0)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="seriesSequenceNew" ng-blur="setSeries(0)"></td>
            <td></td>
        </tr>
        </tbody>
    </table>
    {{-- タイトル(software)テーブル --}}
    <table id="software" ng-show="series_id && !software_detail">
        <caption>
            @{{ series_name }}<button class="btn btn-danger btn-small" ng-click="series_id=false">戻す</button>
        </caption>
        <thead>
        <tr>
            <th>タイトル名</th>
            <th>順番</th>
            <th>各コンテンツ</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="software in softwares">
            <td class="name"><input type="text" class="form-control" ng-model="software.name" ng-blur="setSoftware(software.id)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="software.sequence" ng-blur="setSoftware(software.id)"></td>
            <td class="software">
                <button class="btn btn-info btn-small" ng-click="getSoftwareHardware(software.id)">ハード</button>
                <button class="btn btn-success btn-small" ng-click="getSoftwareBook(software.id)">攻略本</button>
                <button class="btn btn-primary btn-small" ng-click="getSoftwareSoundware(software.id)">サウンド</button>
            </td>
        </tr>
        <tr>
            <td class="name"><input type="text" class="form-control" ng-model="softwareNameNew" ng-blur="setSoftware(0)"></td>
            <td class="sequence"><input type="number" class="form-control" ng-model="softwareSequenceNew" ng-blur="setSoftware(0)"></td>
            <td></td>
        </tr>
        </tbody>
    </table>

    {{-- ソフトウェア--ハードウェアテーブル --}}
    <table id="software_hardware" ng-show="software_detail=='hardware'">
        <caption>@{{ software_name }}<button class="btn btn-danger btn-small" ng-click="software_detail=false">戻す</button></caption>
        <thead>
        <tr>
            <th>ハード名</th>
            <th>商品コード</th>
            <th>価格</th>
            <th>発売日</th>
            <th>備考</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="softwareHardware in softwareHardwares">
            <td class="name">
                <select class="form-control" ng-model="softwareHardware.hardware_id" ng-options="hardware.id as hardware.name for hardware in hardwares" ng-change="setSoftwareHardware(softwareHardware.id)">
                    <option value="">削除</option>
                </select>
            </td>
            <td class="item_code"><input type="text" class="form-control" ng-model="softwareHardware.item_code" ng-blur="setSoftwareHardware(softwareHardware.id)"></td>
            <td class="price"><input type="number" class="form-control" ng-model="softwareHardware.price" ng-blur="setSoftwareHardware(softwareHardware.id)"></td>
            <td class="release_date"><input type="date" class="form-control" ng-model="softwareHardware.release_date" ng-blur="setSoftwareHardware(softwareHardware.id)"></td>
            <td class="remark"><input type="text" class="form-control" ng-model="softwareHardware.remark" ng-blur="setSoftwareHardware(softwareHardware.id)"></td>
        </tr>
        <tr>
            <td class="name">
                <select class="form-control" ng-model="newHardwareId" ng-options="hardware.id as hardware.name for hardware in hardwares" ng-change="setSoftwareHardware(0)">
                    <option value="">削除</option>
                </select>
            </td>
            <td class="item_code"><input type="text" class="form-control" ng-model="newItemcode" ng-blur="setSoftwareHardware(0)"></td>
            <td class="price"><input type="number" class="form-control" ng-model="newPrice" ng-blur="setSoftwareHardware(0)"></td>
            <td class="name"><input type="date" class="form-control" ng-model="newReleasedate" ng-blur="setSoftwareHardware(0)"></td>
            <td class="remark"><input type="text" class="form-control" ng-model="newRemark" ng-blur="setSoftwareHardware(0)"></td>
        </tr>
        </tbody>
    </table>

    {{-- ソフトウェア--攻略本テーブル --}}
    <table id="software_book" ng-show="software_detail=='book'">
        <caption>@{{ software_name }}<button class="btn btn-danger btn-small" ng-click="software_detail=false">戻す</button></caption>
        <thead>
        <tr>
            <th>攻略本名</th>
            <th>商品コード</th>
            <th>価格</th>
            <th>発売日</th>
            <th>備考</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="softwareBook in softwareBooks">
            <td class="name">
                <select class="form-control" ng-model="softwareBook.book_id" ng-options="book.id as book.name for book in books" ng-change="setSoftwareBook(softwareBook.id)">
                    <option value="">削除</option>
                </select>
            </td>
            <td class="item_code"><input class="form-control" type="text" ng-model="softwareBook.item_code" ng-blur="setSoftwareBook(softwareBook.id)"></td>
            <td class="price"><input class="form-control" type="number" ng-model="softwareBook.price" ng-blur="setSoftwareBook(softwareBook.id)"></td>
            <td class="release_date"><input class="form-control" type="date" ng-model="softwareBook.release_date" ng-blur="setSoftwareBook(softwareBook.id)"></td>
            <td class="remark"><input class="form-control" type="text" ng-model="softwareBook.remark" ng-blur="setSoftwareBook(softwareBook.id)"></td>
        </tr>
        <tr>
            <td class="name">
                <select class="form-control" ng-model="newBookId" ng-options="book.id as book.name for book in books" ng-change="setSoftwareBook(0)">
                    <option value="">削除</option>
                </select>
            </td>
            <td class="item_code"><input class="form-control" type="text" ng-model="newItemcode" ng-blur="setSoftwareBook(0)"></td>
            <td class="price"><input class="form-control" type="number" ng-model="newPrice" ng-blur="setSoftwareBook(0)"></td>
            <td class="release_date"><input class="form-control" type="date" ng-model="newReleasedate" ng-blur="setSoftwareBook(0)"></td>
            <td class="remark"><input class="form-control" type="text" ng-model="newRemark" ng-blur="setSoftwareBook(0)"></td>
        </tr>
        </tbody>
    </table>

    {{-- ソフトウェア--サウンドウェアテーブル --}}
    <table id="software_soundware" ng-show="software_detail=='soundware'">
        <caption>@{{ software_name }}<button class="btn btn-danger btn-small" ng-click="software_detail=false">戻す</button></caption>
        <thead>
        <tr>
            <th>サウンドシリーズ名</th>
            <th>商品コード</th>
            <th>価格</th>
            <th>発売日</th>
            <th>備考</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="softwareSoundware in softwareSoundwares">
            <td class="name">
                <select class="form-control" ng-model="softwareSoundware.soundware_id" ng-options="soundware.id as soundware.name for soundware in soundwares" ng-change="setSoftwareSoundware(softwareSoundware.id)">
                    <option value="">削除</option>
                </select>
            </td>
            <td class="item_code" class="item_code"><input type="text" class="form-control" ng-model="softwareSoundware.item_code" ng-blur="setSoftwareSoundware(softwareSoundware.id)"></td>
            <td class="price"><input type="number" class="form-control" ng-model="softwareSoundware.price" ng-blur="setSoftwareSoundware(softwareSoundware.id)"></td>
            <td class="release_date"><input type="date" class="form-control" class="form-control" ng-model="softwareSoundware.release_date" ng-blur="setSoftwareSoundware(softwareSoundware.id)"></td>
            <td class="remark"><input type="text" ng-model="softwareSoundware.remark" ng-blur="setSoftwareSoundware(softwareSoundware.id)"></td>
        </tr>
        <tr>
            <td class="name">
                <select class="form-control" ng-model="newSoundwareId" ng-options="soundware.id as soundware.name for soundware in soundwares" ng-change="setSoftwareSoundware(0)">
                    <option value="">削除</option>
                </select>
            </td>
            <td class="item_code"><input type="text" class="form-control" ng-model="newItemcode" ng-blur="setSoftwareSoundware(0)"></td>
            <td class="price"><input type="number" class="form-control" ng-model="newPrice" ng-blur="setSoftwareSoundware(0)"></td>
            <td class="release_date"><input type="date" class="form-control" ng-model="newReleasedate" ng-blur="setSoftwareSoundware(0)"></td>
            <td class="remark"><input type="text" class="form-control" ng-model="newRemark" ng-blur="setSoftwareSoundware(0)"></td>
        </tr>
        </tbody>
    </table>
</div>
