@extends('layout.common')
@include('layout.head')
@include('layout.header')
@include('layout.footer')

@section('content')
    <div id="content">
        {{-- シリーズ --}}
        <div id="series_list">
            <button class="btn btn-primary" ng-repeat="series in serieses" ng-show="!series_id" ng-click="clickSeries(series.id)">@{{series.name}}</button>
            <button class="btn btn-primary hover" ng-show="series_id" ng-click="clickSeries(0)">@{{series_name}}</button>
        </div>

        {{-- ソフトウェア --}}
        <div id="software_list" ng-show="series_id">
            <button ng-repeat="software in softwares" class="btn btn-success" ng-repeat="software in softwares" ng-show="!software_id" ng-click="clickSoftware(software.id)">@{{software.name}}</button>
            <button class="btn btn-success hover" ng-show="software_id" ng-click="clickSoftware(0)">@{{software_name}}</button>
        </div>
        <table id="software_content_list" ng-show="software_id">
            {{-- ソフトウェア名 --}}
            <thead>
            <tr>
                <th colspan="3">@{{ software_name }}</th>
            </tr>
            </thead>
            {{-- ハードウェア --}}
            <tbody id="software_hardware">
            <tr>
                <th>ハード</th>
                <th>発売日</th>
                <th>備考</th>
            </tr>
            <tr ng-repeat="softwareHardware in softwareHardwares">
                <td>@{{softwareHardware.name}}</td>
                <td>@{{softwareHardware.release_date}}</td>
                <td>@{{softwareHardware.remark}}</td>
            </tr>
            </tbody>
            {{-- 攻略本 --}}
            <tbody id="software_book">
            <tr>
                <th>攻略本</th>
                <th>発売日</th>
                <th>備考</th>
            </tr>
            <tr ng-repeat="softwareBook in softwareBooks">
                <td>@{{softwareBook.name}}</td>
                <td>@{{softwareBook.release_date}}</td>
                <td>@{{softwareBook.remark}}</td>
            </tr>
            </tbody>
            {{-- サウンドウェア --}}
            <tbody id="software_soundware">
            <tr>
                <th>サウンドソフト</th>
                <th>発売日</th>
                <th>備考</th>
            </tr>
            <tr ng-repeat="softwareSoundware in softwareSoundwares">
                <td>@{{softwareSoundware.name}}</td>
                <td>@{{softwareSoundware.release_date}}</td>
                <td>@{{softwareSoundware.remark}}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
