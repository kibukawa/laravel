angular.module('myApp',[])
    .controller('MyController',['$scope','$http',function($scope,$http){

        //ページ読み込み時
        $scope.serieses = [];

        //Ajaxでシリーズ一覧を取得
        getSeries();

        //Ajaxでハードウェア一覧を取得
        getHardware();

        //Ajaxで攻略本一覧を取得
        getBook();

        //Ajaxでサウンドウェア一覧を取得
        getSoundware();

        //シリーズ:登録/更新
        $scope.setSeries = function(id){
            var name;
            var sequence;

            //登録の場合
            if(!id){
                //名前と順番両方埋められていなければリターン
                if(!$scope.seriesNameNew || !$scope.seriesSequenceNew){
                    return false;
                }
                name = $scope.seriesNameNew;
                sequence = $scope.seriesSequenceNew;
            }

            //更新の場合
            if(id){
                $scope.serieses.forEach(function(series){
                    if(series.id==id){
                        name = series.name;
                        sequence = series.sequence;
                    }
                });
            }

            //値が入力されていたら登録
            if(true){
                //Ajaxでシリーズを更新
                $http({
                    method: 'POST',
                    url: '/ajax/setSeries',
                    params:{
                        id:id,
                        name:name,
                        sequence:sequence
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //シリーズを再取得
                    getSeries();

                    //新規登録欄消去
                    $scope.seriesNameNew = '';
                    $scope.seriesSequenceNew = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        };

        //Ajaxでシリーズ一覧を取得
        function getSeries(){
            $http({
                method: 'POST',
                url: '/ajax/getSeries',
                params:{}
            }).then(function onSuccess(data,status,headers,config){
                $scope.serieses = data.data;
            },function onError(data,status,headers,config){
                $scope.serieses = [];
            });
        }

        //Ajaxでタイトル(software)一覧を取得
        $scope.getSoftware = function(series_id){
            $scope.series_id = series_id;
            $scope.serieses.forEach(function(series){
                if(series.id==series_id){
                    $scope.series_name = series.name;
                }
            });

            $http({
                method: 'GET',
                url: '/ajax/getSoftware',
                params:{
                    series_id:series_id
                }
            }).then(function onSuccess(data,status,headers,config){
                $scope.softwares = data.data;
            },function onError(data,status,headers,config){
                $scope.softwares = [];
            });
        }

        //タイトル(Software):登録/更新
        $scope.setSoftware = function(id){
            var name;
            var sequence;

            //登録の場合
            if(!id){
                //名前と順番両方埋められていなければリターン
                if(!$scope.softwareNameNew || !$scope.softwareSequenceNew) {
                    console.log($scope.softwareNameNew + ':' + $scope.softwareSequenceNew);
                    return false;
                }
                name = $scope.softwareNameNew;
                sequence = $scope.softwareSequenceNew;
            }

            //更新の場合
            if(id){
                $scope.softwares.forEach(function(software){
                    if(software.id==id){
                        name = software.name;
                        sequence = software.sequence;
                    }
                });
            }

            //バリッドチェック(未実装):値が正しければ登録/更新
            if(true){
                //Ajaxでタイトル(software)を更新
                $http({
                    method: 'POST',
                    url: '/ajax/setSoftware',
                    params:{
                        id:id,
                        series_id:$scope.series_id,
                        name:name,
                        sequence:sequence
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //タイトル(software)を再取得
                    $scope.getSoftware($scope.series_id);

                    //新規登録欄消去
                    $scope.softwareNameNew = '';
                    $scope.softwareSequenceNew = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //Ajaxでハードウェア一覧を取得
        function getHardware(){
            $http({
                method: 'POST',
                url: '/ajax/getHardware',
                params:{}
            }).then(function onSuccess(data,status,headers,config){
                $scope.hardwares = data.data;
            },function onError(data,status,headers,config){
                $scope.hardwares = [];
            });
        }

        //ハードウェア:登録/更新
        $scope.setHardware = function(id){
            var name;
            var sequence;

            //登録の場合
            if(!id){
                //名前と順番両方埋められていなければリターン
                if(!$scope.hardwareNameNew || !$scope.hardwareSequenceNew){
                    return false;
                }
                name = $scope.hardwareNameNew;
                sequence = $scope.hardwareSequenceNew;
            }

            //更新の場合
            if(id){
                $scope.hardwares.forEach(function(hardware){
                    if(hardware.id==id){
                        name = hardware.name;
                        sequence = hardware.sequence;
                    }
                });
            }

            //バリッドチェック(未実装)がOKなら登録/更新
            if(true){
                //Ajaxでハードウェアを更新
                $http({
                    method: 'POST',
                    url: '/ajax/setHardware',
                    params:{
                        id:id,
                        name:name,
                        sequence:sequence
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //ハードウェアを再取得
                    getHardware();

                    //新規登録欄消去
                    $scope.hardwareNameNew = '';
                    $scope.hardwareSequenceNew = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //Ajaxで攻略本一覧を取得
        function getBook(){
            $http({
                method: 'POST',
                url: '/ajax/getBook',
                params:{}
            }).then(function onSuccess(data,status,headers,config){
                $scope.books = data.data;
            },function onError(data,status,headers,config){
                $scope.books = [];
            });
        }

        //攻略本:登録/更新
        $scope.setBook = function(id){
            var name;
            var sequence;

            //登録の場合
            if(!id){
                //名前と順番両方埋められていなければリターン
                if(!$scope.bookNameNew || !$scope.bookSequenceNew){
                    return false;
                }
                name = $scope.bookNameNew;
                sequence = $scope.bookSequenceNew;
            }

            //更新の場合
            if(id){
                $scope.books.forEach(function(book){
                    if(book.id==id){
                        name = book.name;
                        sequence = book.sequence;
                    }
                });
            }


            //バリッドチェック(未実装)がOKなら登録/更新
            if(true){
                //Ajaxで攻略本を更新
                $http({
                    method: 'POST',
                    url: '/ajax/setBook',
                    params:{
                        id:id,
                        name:name,
                        sequence:sequence
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //攻略本を再取得
                    getBook();

                    //新規登録欄消去
                    $scope.bookNameNew = '';
                    $scope.bookSequenceNew = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //サウンドウェア一覧:取得
        function getSoundware(){
            $http({
                method: 'POST',
                url: '/ajax/getSoundware',
                params:{}
            }).then(function onSuccess(data,status,headers,config){
                $scope.soundwares = data.data;
            },function onError(data,status,headers,config){
                $scope.soundwares = [];
            });
        }

        //サウンドウェア:登録/更新
        $scope.setSoundware = function(id){
            var name;
            var sequence;

            //登録
            if(!id){
                //名前と順番両方埋められていなければリターン
                if(!$scope.soundwareNameNew || !$scope.soundwareSequenceNew){
                    return false;
                }
                name = $scope.soundwareNameNew;
                sequence = $scope.soundwareSequenceNew;
            }

            //更新
            if(id){
                $scope.soundwares.forEach(function(soundware){
                    if(soundware.id==id){
                        name        = soundware.name;
                        sequence    = soundware.sequence;
                    }
                });
            }

            //バリッドチェック(未実装)

            //値が入力されていたら登録
            if(true){
                //Ajaxで攻略本を更新
                $http({
                    method: 'POST',
                    url: '/ajax/setSoundware',
                    params:{
                        id      :id,
                        name    :name,
                        sequence:sequence
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //攻略本を再取得
                    getSoundware();

                    //新規登録情報削除
                    $scope.soundwareNameNew = '';
                    $scope.soundwareSequenceNew = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //Ajaxでソフトウェア--ハードウェア一覧を取得
        $scope.getSoftwareHardware = function(software_id){
            $scope.software_id = software_id;
            $scope.software_detail = 'hardware';
            $scope.softwares.forEach(function(software){
                if(software.id==software_id){
                    $scope.software_name = software.name;
                }
            });

            $http({
                method: 'POST',
                url: '/ajax/getSoftwareHardware',
                params:{
                    software_id: software_id
                }
            }).then(function onSuccess(data,status,headers,config){
                $scope.softwareHardwares = data.data;

                //release_dateをDate型にParseする(未実装)
                for(var key in $scope.softwareHardwares){
                    var release_date = $scope.softwareHardwares[key].release_date;
                    $scope.softwareHardwares[key].release_date = new Date(release_date);
                }
            },function onError(data,status,headers,config){
                $scope.softwareHardwares = [];
            });
        }

        //ソフトウェア--ハードウェア:登録/更新
        $scope.setSoftwareHardware = function(id){
            var software_id;
            var hardware_id;
            var item_code;
            var price;
            var release_date;
            var remark;

            //登録
            if(!id){
                software_id = $scope.software_id;
                hardware_id = $scope.newHardwareId;
                item_code   = $scope.newItemCode;
                price       = $scope.newPrice;
                release_date = $scope.release_date;
                remark      = $scope.remark;

                //ハードウェアIDがなければリターン
                if(!hardware_id){
                    return false;
                }
            }

            //更新
            if(id){
                $scope.softwareHardwares.forEach(function(softwareHardware){
                    if(softwareHardware.id==id){
                        software_id = softwareHardware.software_id;
                        hardware_id = softwareHardware.hardware_id;
                        item_code   = softwareHardware.item_code;
                        price       = softwareHardware.price;
                        release_date = softwareHardware.release_date;
                        remark      = softwareHardware.remark;
                    }
                });
                if(!hardware_id){
                    hardware_id = 0;
                }
            }

            //バリッドチェック(未実装)

            //登録/更新
            if(true){
                $http({
                    method: 'POST',
                    url: '/ajax/setSoftwareHardware',
                    params:{
                        id           : id,
                        software_id  : software_id,
                        hardware_id  : hardware_id,
                        item_code    : item_code,
                        price        : price,
                        release_date : release_date,
                        remark       : remark
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //ソフトウェア--ハードウェアを再取得
                    $scope.getSoftwareHardware(software_id);

                    //新規登録欄消去
                    $scope.newHardwareId = '';
                    $scope.newItemCode = '';
                    $scope.newPrice = '';
                    $scope.release_date = '';
                    $scope.remark = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //Ajaxでソフトウェア--攻略本一覧を取得
        $scope.getSoftwareBook = function(software_id){
            $scope.software_id = software_id;
            $scope.software_detail = 'book';
            $scope.softwares.forEach(function(software){
                if(software.id==software_id){
                    $scope.software_name = software.name;
                }
            });

            $http({
                method: 'POST',
                url: '/ajax/getSoftwareBook',
                params:{
                    software_id: software_id
                }
            }).then(function onSuccess(data,status,headers,config){
                $scope.softwareBooks = data.data;

                //release_dateをDate型にParseする
                for(var key in $scope.softwareBooks){
                    var release_date = $scope.softwareBooks[key].release_date;
                    $scope.softwareBooks[key].release_date = new Date(release_date);
                }
            },function onError(data,status,headers,config){
                $scope.softwareBooks = [];
            });
        }

        //ソフトウェア--攻略本:登録/更新
        $scope.setSoftwareBook = function(id){
            var software_id;
            var book_id;
            var item_code;
            var price;
            var release_date;
            var remark;

            //登録
            if(!id){
                software_id = $scope.software_id;
                book_id     = $scope.newBookId;
                item_code   = $scope.newItemCode;
                price       = $scope.newPrice;
                release_date = $scope.release_date;
                remark      = $scope.remark;

                //攻略本IDがなければリターン
                if(!book_id){
                    return false;
                }
            }

            //更新
            if(id){
                $scope.softwareBooks.forEach(function(softwareBook){
                    if(softwareBook.id==id){
                        software_id = softwareBook.software_id;
                        book_id     = softwareBook.book_id;
                        item_code   = softwareBook.item_code;
                        price       = softwareBook.price;
                        release_date = softwareBook.release_date;
                        remark      = softwareBook.remark;
                    }
                });

                //攻略本IDが設定されていなければ0
                if(!book_id){
                    book_id = 0;
                }
            }

            //バリッドチェック(未実装)

            //登録
            if(true){
                $http({
                    method: 'POST',
                    url: '/ajax/setSoftwareBook',
                    params:{
                        id           : id,
                        software_id  : software_id,
                        book_id      : book_id,
                        item_code    : item_code,
                        price        : price,
                        release_date : release_date,
                        remark       : remark
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //ソフトウェア--ハードウェアを再取得
                    $scope.getSoftwareBook(software_id);

                    //新規登録欄消去
                    $scope.newBookId = '';
                    $scope.newItemCode = '';
                    $scope.newPrice = '';
                    $scope.release_date = '';
                    $scope.remark = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //Ajaxでソフトウェア--サウンドウェア一覧を取得
        $scope.getSoftwareSoundware = function(software_id){
            $scope.software_id = software_id;
            $scope.software_detail = 'soundware';
            $scope.softwares.forEach(function(software){
                if(software.id==software_id){
                    $scope.software_name = software.name;
                }
            });

            $http({
                method: 'POST',
                url: '/ajax/getSoftwareSoundware',
                params:{
                    software_id: software_id
                }
            }).then(function onSuccess(data,status,headers,config){
                $scope.softwareSoundwares = data.data;

                //release_dateをDate型にParseする
                for(var key in $scope.softwareSoundwares){
                    var release_date = $scope.softwareSoundwares[key].release_date;
                    $scope.softwareSoundwares[key].release_date = new Date(release_date);
                }
            },function onError(data,status,headers,config){
                $scope.softwareSoundwares = [];
            });
        }

        //ソフトウェア--サウンドウェア:登録/更新
        $scope.setSoftwareSoundware = function(id){
            var software_id;
            var soundware_id;
            var item_code;
            var price;
            var release_date;
            var remark;

            //登録
            if(!id){
                software_id = $scope.software_id;
                soundware_id     = $scope.newSoundwareId;
                item_code   = $scope.newItemcode;
                price       = $scope.newPrice;
                release_date = $scope.newReleasedate;
                remark      = $scope.newRemark;
                //サウンドウェアIDがなければリターン
                if(!soundware_id){
                    return false;
                }
            }

            //更新
            if(id){
                $scope.softwareSoundwares.forEach(function(softwareSoundware){
                    if(softwareSoundware.id==id){
                        software_id = softwareSoundware.software_id;
                        soundware_id     = softwareSoundware.soundware_id;
                        item_code   = softwareSoundware.item_code;
                        price       = softwareSoundware.price;
                        release_date = softwareSoundware.release_date;
                        remark      = softwareSoundware.remark;
                    }
                });

                //サウンドウェアIDが設定されていなければ0
                if(!soundware_id){
                    soundware_id = 0;
                }
            }

            //バリッドチェック(未実装)

            //登録
            if(true){
                $http({
                    method: 'POST',
                    url: '/ajax/setSoftwareSoundware',
                    params:{
                        id           : id,
                        software_id  : software_id,
                        soundware_id : soundware_id,
                        item_code    : item_code,
                        price        : price,
                        release_date : release_date,
                        remark       : remark
                    }
                }).then(function onSuccess(data,status,headers,config){
                    //正常処理
                    //ソフトウェア--サウンドウェアを再取得
                    $scope.getSoftwareSoundware(software_id);

                    //新規登録欄消去
                    $scope.newSoundwareId = '';
                    $scope.newItemCode = '';
                    $scope.newPrice = '';
                    $scope.release_date = '';
                    $scope.remark = '';
                },function onError(data,status,headers,config){
                    //エラー処理
                });
            }
        }

        //TOPページでシリーズ名をクリック
        $scope.clickSeries = function(id){
            $scope.series_id = id;
            if(id==0){
                $scope.software_id = 0;
            }
            //ソフトウェア一覧を取得
            $scope.getSoftware(id);
        }
        //TOPページでソフト名をクリック
        $scope.clickSoftware = function(id){
            $scope.software_id = id;
            //ソフトウェア--ハードウェア一覧を取得
            $scope.getSoftwareHardware(id);
            //ソフトウェア--攻略本一覧を取得
            $scope.getSoftwareBook(id);
            //ソフトウェア--サウンドウェア一覧を取得
            $scope.getSoftwareSoundware(id);
        }
    }]);

