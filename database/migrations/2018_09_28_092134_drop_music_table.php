<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMusicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //musicテーブル削除
        Schema::dropIfExists('music');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //musicテーブル作成
        Schema::create('music', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('software_id')->default(null);
            $table->string('name')->default(null);
            $table->string('item_code')->default(null);
            $table->integer('price')->default(null);
            $table->dateTime('release_date')->default(null);
            $table->timestamps();
        });
    }
}
