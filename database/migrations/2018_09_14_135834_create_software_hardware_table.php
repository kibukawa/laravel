<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareHardwareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('software_hardware', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('software_id')->default(null);
            $table->integer('hardware_id')->default(null);
            $table->string('item_code')->default(null);
            $table->integer('price')->default(null);
            $table->dateTime('release_date')->default(null);
            $table->string('remark')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_hardware');
    }
}
