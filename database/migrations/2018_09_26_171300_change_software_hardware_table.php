<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSoftwareHardwareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('software_hardware', function (Blueprint $table) {
            //データ型の変更
            $table->date('release_date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('software_hardware', function (Blueprint $table) {
            //データ型の変更
            $table->dateTime('release_date')->change();
        });
    }
}
