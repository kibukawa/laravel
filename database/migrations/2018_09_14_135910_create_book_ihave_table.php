<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookIhaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_ihave', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenre')->default(null);
            $table->integer('jenre_id')->default(null);
            $table->boolean('have')->default(false);
            $table->integer('want')->default(null);
            $table->dateTime('have_date')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_ihave');
    }
}
