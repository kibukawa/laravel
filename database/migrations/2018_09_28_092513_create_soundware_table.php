<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoundwareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //soundwareテーブル作成
        Schema::create('soundware', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sequence')->default(null);
            $table->boolean('deleted_flag')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //soundwareテーブル削除
        Schema::dropIfExists('soundware');
    }
}
