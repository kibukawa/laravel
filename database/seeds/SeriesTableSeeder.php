<?php

use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $param = [
            'name'  => '信長の野望',
            'sequence'  => '1',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('series')->insert($param);

        $param = [
            'name'  => '三國志',
            'sequence'  => '2',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('series')->insert($param);

        $param = [
            'name'  => '蒼き狼と白き牝鹿',
            'sequence'  => '3',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('series')->insert($param);
    }
}
