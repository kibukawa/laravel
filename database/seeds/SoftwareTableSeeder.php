<?php

use Illuminate\Database\Seeder;

class SoftwareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $param = [
            'series_id'  => '1',
            'name'  => '全国版',
            'sequence'  => '1',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('software')->insert($param);

        $param = [
            'series_id'  => '1',
            'name'  => '戦国群雄伝',
            'sequence'  => '2',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('software')->insert($param);

    }
}
