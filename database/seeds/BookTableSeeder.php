<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $param = [
            'name'  => 'ハンドブック',
            'sequence'  => '1',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);

        $param = [
            'name'  => 'ガイドブック',
            'sequence'  => '2',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);

        $param = [
            'name'  => 'スーパーガイドブック',
            'sequence'  => '3',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);

        $param = [
            'name'  => 'ハイパーガイドブック',
            'sequence'  => '4',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);

        $param = [
            'name'  => 'マスターブック',
            'sequence'  => '5',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);

        $param = [
            'name'  => '武将FILE',
            'sequence'  => '6',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);

        $param = [
            'name'  => '好漢FILE',
            'sequence'  => '7',
            'created_at'  => new DateTime(),
            'updated_at'  => new DateTime(),
        ];
        DB::table('book')->insert($param);
    }
}
