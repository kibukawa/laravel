<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//管理ページ
Route::get('/admin/','AdminController@index');

//Ajaxページ
//シリーズ関連
Route::post('/ajax/getSeries','AjaxController@getSeries');
Route::post('/ajax/setSeries','AjaxController@setSeries');

//タイトル(software)関連
Route::get('/ajax/getSoftware','AjaxController@getSoftware');
Route::post('/ajax/setSoftware','AjaxController@setSoftware');

//ハードウェア関連
Route::post('/ajax/getHardware','AjaxController@getHardware');
Route::post('/ajax/setHardware','AjaxController@setHardware');

//攻略本関連
Route::post('/ajax/getBook','AjaxController@getBook');
Route::post('/ajax/setBook','AjaxController@setBook');

//サウンドウェア関連
Route::post('/ajax/getSoundware','AjaxController@getSoundware');
Route::post('/ajax/setSoundware','AjaxController@setSoundware');

//ハードウェア--ソフトウェア関連
Route::post('/ajax/getSoftwareHardware','AjaxController@getSoftwareHardware');
Route::post('/ajax/setSoftwareHardware','AjaxController@setSoftwareHardware');

//ハードウェア--攻略本関連
Route::post('/ajax/getSoftwareBook','AjaxController@getSoftwareBook');
Route::post('/ajax/setSoftwareBook','AjaxController@setSoftwareBook');

//ハードウェア--サウンドウェア関連
Route::post('/ajax/getSoftwareSoundware','AjaxController@getSoftwareSoundware');
Route::post('/ajax/setSoftwareSoundware','AjaxController@setSoftwareSoundware');

